/**
 * @file
 * Define tags as "block level" so the editor doesn't try to put paragraph tags around them.
 */

cktags = ['cktdx'];

// Global variable used to store fakeImage element when socialmedia is double clicked.
var doubleclick_element;

CKEDITOR.plugins.add('embedded_tdx', {
  requires : ['dialog', 'fakeobjects'],
  init: function (editor) {
    // Define plugin name.
    var pluginName = 'embedded_tdx';

    // Set up object to share variables amongst scripts.
    CKEDITOR.embedded_tdx = {};

    // Register button for Team dynamix.
    editor.ui.addButton('TeamDynamix', {
      label : "Add/Edit TeamDynamix",
      command : 'tdx',
      icon: this.path + 'icons/team_dynamix.png',
    });
    // Register right-click menu item for Team dynamix.
    editor.addMenuItems({
      tdx : {
        label : "Edit TeamDynamix",
        icon: this.path + 'icons/team_dynamix.png',
        command : 'tdx',
        group : 'image',
        order : 1
      }
    });
    // Make cktdx to be a self-closing tag.
    CKEDITOR.dtd.$empty['cktdx'] = 1;
    // Make sure the fake element for Team dynamix has a name.
    CKEDITOR.embedded_tdx.cktdx = 'tdx';
    CKEDITOR.lang.en.fakeobjects.cktdx = CKEDITOR.embedded_tdx.cktdx;

    // Add JavaScript file that defines the dialog box for Team dynamix.
    CKEDITOR.dialog.add('tdx', this.path + 'dialogs/teamdynamix.js');
    // Register command to open dialog box when button is clicked.
    editor.addCommand('tdx', new CKEDITOR.dialogCommand('tdx'));

    // Regular expressions for Team dynamix.
    CKEDITOR.embedded_tdx.tdx_regex = /^[1-9]\d{3,}$/;
    // Open the appropriate dialog box if an element is double-clicked.
    editor.on('doubleclick', function (evt) {
      var element = evt.data.element;

      // Store the element as global variable to be used in onShow of dialog, when double clicked.
      doubleclick_element = element;

      if (element.is('img') && element.data('cke-real-element-type') === 'cktdx') {
        evt.data.dialog = 'tdx';
      }
    });

    // Add the appropriate right-click menu item if an element is right-clicked.
    if (editor.contextMenu) {
      editor.contextMenu.addListener(function (element, selection) {
        if (element && element.is('img') && element.data('cke-real-element-type') === 'cktdx') {
          return { tdx : CKEDITOR.TRISTATE_OFF };
        }
      });
    }

    // Add CSS to use in-editor to style custom (fake) elements.
    CKEDITOR.addCss(

      'img.cktdx {' +
      'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/team_dynamix.png') + ');' +
      'background-position: center center;' +
      'background-repeat: no-repeat;' +
      'background-color: #000;' +
      'width: 100%;' +
      'height: 100px;' +
      'margin: 0 0 10px 0;' +
      'margin-left: auto;' +
      'margin-right: auto;' +
    '}' +

      // Team dynamix: Definitions for wide width.
        '.uw_tf_standard_wide p img.cktdx {' +
          'height: 200px;' +
        '}' +

        // Team dynamix: Definitions for standard width.
        '.uw_tf_standard p img.tdx {' +
          'height: 200px;' +
        '}'
    );
  },
  afterInit : function (editor) {
    // Make fake image display on first load/return from source view.
    if (editor.dataProcessor.dataFilter) {
      editor.dataProcessor.dataFilter.addRules({
        elements : {
          cktdx : function (element) {
            // Reset title.
            CKEDITOR.lang.en.fakeobjects.cktdx = CKEDITOR.embedded_tdx.cktdx;
            // Adjust title if a list name is present.
            if (element.attributes['data-tdx-id']) {
              CKEDITOR.lang.en.fakeobjects.cktdx = 'TeamDynamix: ' + element.attributes['data-tdx-id'];
            }
            // Note that this just accepts whatever attributes are on the element; may want to filter these.
            return editor.createFakeParserElement(element, 'cktdx', 'cktdx', false);
          }
        }
      });
    }
  }
});