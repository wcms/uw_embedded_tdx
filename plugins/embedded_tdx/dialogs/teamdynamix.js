/**
 * @file
 */

(function () {
  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;
  var tdxDialog = function (editor) {
    return {
      title : 'TeamDynamix Properties',
      minWidth : 625,
      minHeight : 150,
      contents: [{
        id: 'tdx',
        label: 'tdx',
        elements:[{
          type: 'text',
          id: 'tdx-id',
          label: 'Please enter a TeamDynamix report ID:',
          required: true,
          setup: function (element) {
            this.setValue(element.getAttribute('data-tdx-id'));
          }
        },{
          type: 'select',
          id: 'tdx-show-project-title',
          label: 'Show project title:',
          items: [ [ 'Yes' ], [ 'No' ]], 'default': 'Yes',
          required: true,
          setup: function (element) {
            this.setValue(element.getAttribute('data-tdx-show-project-title'));
          }
        }]
      }],
      onOk: function () {
        // Get form information.
        tdx_id = this.getValueOf('tdx','tdx-id');
        tdx_show_project_title = this.getValueOf('tdx', 'tdx-show-project-title');

        // Validate input. Note that there is probably a CKEditor specific way to do this, but this works.
        errors = "";
        if (!CKEDITOR.embedded_tdx.tdx_regex.test(tdx_id)) {
            errors += "You must enter a valid report ID.\r\n";
        }
        else {
          errors = '';
        }
        if (!tdx_id) {
          errors = "You must enter a report ID.\r\n";
        }

        // Ensure that form has not been submitted before.
        if (formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
        // Only display errors if there are errors to display and the form has not been run before.
         else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        }
        else {
          // Create the TeamDynamix element.
          var cktdxNode = new CKEDITOR.dom.element('cktdx');
          // Save contents of dialog as attributes of the element.
          cktdxNode.setAttribute('data-tdx-id', tdx_id);
          cktdxNode.setAttribute('data-tdx-show-project-title', tdx_show_project_title);

          // Adjust title based on user input.
          CKEDITOR.lang.en.fakeobjects.cktdx = 'TeamDynamix: ' + tdx_id;
          // Create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable)
          var newFakeImage = editor.createFakeElement(cktdxNode, 'cktdx', 'cktdx', false);
          // Set the fake object to the entered height; if there isn't one, use 100 so it's not invisible.
          newFakeImage.addClass('cktdx');
          if (this.fakeImage) {
            newFakeImage.replace(this.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
          }
          else {
            editor.insertElement(newFakeImage);
          }
          // Reset title.
          CKEDITOR.lang.en.fakeobjects.cktdx = CKEDITOR.embedded_tdx.cktdx;
        }
      },
      onShow: function () {
        // Set up to handle existing items.
        this.fakeImage = this.cktdxNode = null;
        var fakeImage;

        // Check if element is right clicked or icon was clicked, if not use global varaible doubleclick_element.
        if (this.getSelectedElement()) {
          fakeImage = this.getSelectedElement();
        }
        else {
          fakeImage = fakeImage;
        }

        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'cktdx') {
          this.fakeImage = fakeImage;
          var cktdxNode = editor.restoreRealElement(fakeImage);
          this.cktdxNode = cktdxNode;
          this.setupContent(cktdxNode);
        }
      }
    }
  }

  CKEDITOR.dialog.add('tdx', function (editor) {
    return tdxDialog(editor);
  });
})();
