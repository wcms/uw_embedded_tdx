CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Design decisions

INTRODUCTION
------------

Embedded TDX (Team Dynamix) is a Drupal module that allow users to import reports from the TDX API and place them on a page.

INSTALLATION
------------

The module is like most Drupal modules but requires a bit more effort than normal to get going.

1. This module REQUIRES the BEID and Web Services keys should be stored in a settings file (on WCMS servers, password_settings.php).  The variable names are: tdx_beid and tdx_web_services_key.

DESIGN DECISIONS
----------------

1. This module calls the TDX API multiple times per instance of the TDX ck tag.  The API must be called at least twice as the first call requests the bearer token based on the credentials in #1.  This bearer token must then be sent with any other request to the API.

2. The tableheader.js is included on hook_page_alter to ensure that it is loaded on every browser on every operating system.  If it is added during the ckeditor process some browsers on some operating systems will not load the tableheader.js.
